package br.edu.infnet.botbuilder.client;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.gwt.GwtApplication;
import com.badlogic.gdx.backends.gwt.GwtApplicationConfiguration;
import br.edu.infnet.botbuilder.BotBuilder;
import br.edu.infnet.botbuilder.constants.ScreenConstant;

public class HtmlLauncher extends GwtApplication {

        @Override
        public GwtApplicationConfiguration getConfig () {
                return new GwtApplicationConfiguration(ScreenConstant.V_WIDTH, ScreenConstant.V_HEIGHT);
        }

        @Override
        public ApplicationListener createApplicationListener () {
                return new BotBuilder();
        }
}