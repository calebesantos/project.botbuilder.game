package br.edu.infnet.botbuilder.scenes;

import java.util.HashMap;
import java.util.Map.Entry;

import com.badlogic.gdx.scenes.scene2d.ui.Table;

import br.edu.infnet.botbuilder.sprites.Player;
import br.edu.infnet.botbuilder.sprites.PlayerOpponent;

public class TableHealthBar {

	private Table table;
	private HashMap<Player, HealthBar> playersHealths;
	private HashMap<PlayerOpponent, HealthBar> playersOpponentsHealths;
	
	public TableHealthBar() {
		
		table = new Table();
		table.bottom().padBottom(10);
		table.setFillParent(true);
		
		playersHealths = new HashMap<>();
		playersOpponentsHealths = new HashMap<>();
	}
	
	public void addPlayer(Player player){
		
		HealthBar healthBar = new HealthBar(table);
		healthBar.initHealth(player.getLife());
		playersHealths.put(player, healthBar);
		
		if (table.getCells().size % 4 == 0)
			table.row().padBottom(10);

		table.add(healthBar.getProgressBar()).expandX();
	}
	
	public void update(float delta) {
		for (Entry<Player, HealthBar> entry : playersHealths.entrySet()) {
			Player player = entry.getKey();
			HealthBar healthBar = entry.getValue();
			
			healthBar.update(delta, player.getLife());
		}
		
		for (Entry<PlayerOpponent, HealthBar> entry : playersOpponentsHealths.entrySet()) {
			PlayerOpponent player = entry.getKey();
			HealthBar healthBar = entry.getValue();
			
			healthBar.update(delta, player.getLife());
		}
	}
	
	public Table getTable() {
		return table;
	}

	public void addPlayer(PlayerOpponent opponent) {
		HealthBar healthBar = new HealthBar(table);
		healthBar.initHealth(opponent.getLife());
		playersOpponentsHealths.put(opponent, healthBar);
		startTable();
	}
	
	public void removePlayer(PlayerOpponent opponent) {
		playersOpponentsHealths.remove(opponent);
		startTable();
	}
	
	public void startTable() {
		
		int sizeTable = table.getCells().size;
		
		if(sizeTable > 0)
			table.clearChildren();
		
		for (Entry<Player, HealthBar> entry : playersHealths.entrySet()) {
			
			HealthBar healthBar = entry.getValue();
			
			if (sizeTable % 4 == 0)
				table.row().padBottom(10);
	
			table.add(healthBar.getProgressBar()).expandX();
		}
		
		for (Entry<PlayerOpponent, HealthBar> entry : playersOpponentsHealths.entrySet()) {
			
			HealthBar healthBar = entry.getValue();
			
			if (sizeTable % 4 == 0)
				table.row().padBottom(10);
	
			table.add(healthBar.getProgressBar()).expandX();
		}
	}
}
