package br.edu.infnet.botbuilder.scenes;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar.ProgressBarStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class HealthBar {

	private ProgressBar progressBar;
	private TextureRegionDrawable textureBar;
	private float health;
	private static float positionX;

	public HealthBar(Table table) {

		if (positionX == 0)
			positionX = 10;
		else
			positionX += 150;

		Skin skin = new Skin();
		Pixmap pixmap = new Pixmap(10, 20, Format.RGBA8888);
		pixmap.setColor(Color.WHITE);
		pixmap.fill();
		skin.add("white", new Texture(pixmap));

		Pixmap pixmap2 = new Pixmap(10, 20, Format.RGBA8888);
		pixmap2.setColor(Color.BLACK);
		pixmap2.fill();

		textureBar = new TextureRegionDrawable(new TextureRegion(new Texture(pixmap2)));

		ProgressBarStyle style = new ProgressBar.ProgressBarStyle(skin.newDrawable("white", Color.WHITE), textureBar);
		style.knobBefore = style.knob;

		progressBar = new ProgressBar(0, 100, 1, false, style);
		progressBar.setBounds(positionX, 10, 100, progressBar.getPrefHeight());

		Label textLabel = new Label("playerName", new Label.LabelStyle(new BitmapFont(), Color.WHITE));
		textLabel.setPosition(progressBar.getX(), progressBar.getY() + 20);

//		if (table.getCells().size % 4 == 0)
//			table.row().padBottom(10);
//
//		table.add(progressBar).expandX();
	}

	public ProgressBar getProgressBar() {
		return progressBar;
	}

	public TextureRegionDrawable getTextureBar() {
		return textureBar;
	}

	public void setPosition(float x, float y) {
		progressBar.setPosition(x, y);
	}

	public void initHealth(float health) {
		this.health = health;
	}

	public void updateHealth(float value) {
		health = value;
	}
	
	public float getHealth() {
		return health;
	}

	public void update(float delta, float lifePlayer) {
		// if(health != progressBar.getValue())
		// System.out.println(health);
//		System.out.println(health);
		health = lifePlayer;
		progressBar.setValue(health);
	}

}
