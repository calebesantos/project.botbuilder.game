package br.edu.infnet.botbuilder.constants;

public class ScreenConstant {
	
	public static final float PPM = 100;
	public static final int V_WIDTH = 800;
	public static final int V_HEIGHT = 600;
	
	public static float toPPM(float value) {
		return value / PPM;
	}
}
