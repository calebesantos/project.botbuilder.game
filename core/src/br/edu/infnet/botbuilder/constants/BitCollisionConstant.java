package br.edu.infnet.botbuilder.constants;

public class BitCollisionConstant {
	public static final short NOTHING_BIT = 0;
	public static final short GROUND_BIT = 1;
	public static final short PLAYER_BIT = 2;
	public static final short PLAYER_ARM_BIT = 4;
	public static final short PLAYER_LEG_BIT = 8;
}
