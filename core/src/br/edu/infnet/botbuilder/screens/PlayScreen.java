package br.edu.infnet.botbuilder.screens;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import br.edu.infnet.botbuilder.constants.ScreenConstant;
import br.edu.infnet.botbuilder.scenes.Hud;
import br.edu.infnet.botbuilder.scenes.TableHealthBar;
import br.edu.infnet.botbuilder.singleton.InputProcessorManagerSingleton;
import br.edu.infnet.botbuilder.singleton.SpriteBatchSingleton;
import br.edu.infnet.botbuilder.sprites.Player;
import br.edu.infnet.botbuilder.sprites.PlayerConfiguration;
import br.edu.infnet.botbuilder.sprites.PlayerOpponent;
import br.edu.infnet.botbuilder.sprites.State;
import br.edu.infnet.botbuilder.tools.Box2DWorldCreator;
import br.edu.infnet.botbuilder.tools.StartListener;
import br.edu.infnet.botbuilder.tools.SubscribeListener;
import br.edu.infnet.botbuilder.tools.WebsocketCustom;
import br.edu.infnet.botbuilder.tools.WorldContactListener;

public class PlayScreen implements Screen, StartListener {

	private static final float UPDATE_TIME = 1 / 60f;
	private OrthographicCamera camera;
	private Viewport viewport;
	private Hud hud;

	// TiledMap variables
	private TmxMapLoader mapLoader;
	private TiledMap map;
	private OrthogonalTiledMapRenderer mapRenderer;
	private TableHealthBar tableHealthBar;

	// Box2d variables
	private World world;
	private Box2DDebugRenderer box2dDebug;

	@SuppressWarnings("unused")
	private Box2DWorldCreator worldCreator;

	// Sprites
	private Player player;

	// SpriteBatch
	private SpriteBatch batch;
	private float stateTimer;

	// Websocket
	private WebsocketCustom websocket;
	private HashMap<Long, PlayerOpponent> playersOpponents;

	public PlayScreen() {

		batch = SpriteBatchSingleton.getInstance();

		camera = new OrthographicCamera();
		viewport = new FitViewport(ScreenConstant.toPPM(ScreenConstant.V_WIDTH) / 4,
				ScreenConstant.toPPM(ScreenConstant.V_HEIGHT) / 4, camera);

		hud = new Hud();
		mapLoader = new TmxMapLoader();
		map = mapLoader.load("botbuilder/level1.tmx");
		mapRenderer = new OrthogonalTiledMapRenderer(map, ScreenConstant.toPPM(1));
		camera.position.set(viewport.getWorldWidth() / 2, viewport.getWorldHeight() / 2, 0);

		world = new World(new Vector2(0, -10), true);
		box2dDebug = new Box2DDebugRenderer();
		worldCreator = new Box2DWorldCreator(this);

		playersOpponents = new HashMap<>();

		/*
		 * player = new Player( new PlayerConfiguration("Player_1",
		 * Input.Keys.Z, Input.Keys.X, Input.Keys.UP, Input.Keys.DOWN,
		 * Input.Keys.RIGHT, Input.Keys.LEFT, getWorld(),
		 * InputProcessorManagerSingleton.getInstance()));
		 * 
		 * playerOpponent = new Player(new PlayerConfiguration("Player_2",
		 * Input.Keys.T, Input.Keys.Y, Input.Keys.I, Input.Keys.K, Input.Keys.L,
		 * Input.Keys.J, getWorld(),
		 * InputProcessorManagerSingleton.getInstance()));
		 */

		tableHealthBar = new TableHealthBar();

		hud.getStage().addActor(tableHealthBar.getTable());

		world.setContactListener(new WorldContactListener());

		websocket = new WebsocketCustom("http://localhost:8282/botbuilder/websocketgame");
		websocket.initialize();
		websocket.connect(this);

		/*
		 * long idOpponent = 10; PlayerOpponent playerOpponent = new
		 * PlayerOpponent( new PlayerConfiguration("Teste.properties",
		 * getWorld(), InputProcessorManagerSingleton.getInstance()));
		 * playersOpponents.put(idOpponent, playerOpponent);
		 * tableHealthBar.addPlayer(playerOpponent);
		 * 
		 * player = new Player( new PlayerConfiguration("Player_1",
		 * Input.Keys.Z, Input.Keys.X, Input.Keys.UP, Input.Keys.DOWN,
		 * Input.Keys.RIGHT, Input.Keys.LEFT, getWorld(),
		 * InputProcessorManagerSingleton.getInstance()));
		 * tableHealthBar.addPlayer(player);
		 */

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

	@Override
	public void render(float delta) {

		if (gameStarted())
			update(delta);

		updateServer(delta);
		mapRenderer.setView(camera);

		clearScreen();

		mapRenderer.render();
		box2dDebug.render(world, camera.combined);

		batch.setProjectionMatrix(camera.combined);

		if (gameStarted()) {
			batch.begin();

			player.draw(batch);
			for (Entry<Long, PlayerOpponent> entry : playersOpponents.entrySet()) {
				entry.getValue().draw(batch);
			}

			batch.end();
		}
		batch.setProjectionMatrix(hud.getStage().getCamera().combined);
		hud.getStage().draw();

	}

	private boolean gameStarted() {
		return player != null;
	}

	public TiledMap getMap() {
		return map;
	}

	public World getWorld() {
		return world;
	}

	private void clearScreen() {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
	}

	private void update(float delta) {

		world.step(1 / 60f, 3, 2);

		player.update(delta);

		for (Entry<Long, PlayerOpponent> entry : playersOpponents.entrySet()) {
			entry.getValue().update(delta);
		}

		tableHealthBar.update(delta);
		hud.update(delta);

		float offsetBeginX = player.getBody().getPosition().x - camera.viewportWidth / 2;
		float offsetEndX = player.getBody().getPosition().x + camera.viewportWidth / 2;

		if (offsetBeginX > 0 && offsetEndX < ScreenConstant.toPPM(ScreenConstant.V_WIDTH / 1.95f))
			camera.position.x = player.getBody().getPosition().x;

		camera.update();
	}

	private void updateServer(float delta) {
		stateTimer += delta;

		/*
		 * if (player == null || stateTimer < UPDATE_TIME) return;
		 * 
		 * int input = player.hasMoved() ? 1 : 0;
		 */

		/*
		 * if (player.isKeyFrontPressed()) input = 1; else if
		 * (player.isKeyBackPressed()) input = 2; else if
		 * (player.isKeyJumpPressed()) input = 3;
		 */
		// String json = "";//parseToJson(player);

		// if(input > 0) {
		if (player != null && player.hasChangedStatus() && stateTimer >= UPDATE_TIME) {
			String json = parseToJson(player);

//			websocket.showConsole(json);
			websocket.send("/app/game", json);
		}
	}

	@Override
	public void resize(int width, int height) {
		viewport.update(width, height);
	}

	@Override
	public void dispose() {
		map.dispose();
		world.dispose();
		box2dDebug.dispose();
		mapRenderer.dispose();
		hud.dispose();
	}

	@Override
	public void init() {
		websocket.subscribe("/topic/newplayer", new SubscribeListener() {
			public void call(String data) {
				JsonValue jsonValue = new JsonReader().parse(data);
				long idOpponent = jsonValue.getLong("id");
				PlayerOpponent playerOpponent = new PlayerOpponent(new PlayerConfiguration("Teste.properties",
						getWorld(), InputProcessorManagerSingleton.getInstance()));
				addPlayerConnected(idOpponent, playerOpponent);
//				websocket.showConsole(data);
			}
		});

		websocket.subscribe("/app/playerid", new SubscribeListener() {

			public void call(String data) {
				PlayerJson player = parseFromJson(data);
				addPlayerLocal(player.id, player.sessionId);
//				websocket.showConsole(data);
			}
		});

		websocket.subscribe("/topic/playermoved", new SubscribeListener() {
			public void call(String data) {
//				websocket.showConsole(data);
				PlayerJson player = parseFromJson(data);
				updatePlayerConnected(player);
			}
		});

		websocket.subscribe("/app/playersconnected", new SubscribeListener() {
			public void call(String data) {

				List<PlayerJson> players = parseFromJsonArray(data);
				for (PlayerJson p : players) {
					PlayerOpponent opponent = new PlayerOpponent(new PlayerConfiguration("Teste.properties", getWorld(),
							InputProcessorManagerSingleton.getInstance()));
					opponent.getBody().setTransform(p.position, 0);
					opponent.setSessionId(p.sessionId);
					addPlayerConnected(p.id, opponent);
//					websocket.showConsole("" + p.id);
				}
			}
		});

		websocket.subscribe("/topic/playerdisconnect", new SubscribeListener() {
			public void call(String data) {
				PlayerJson player = parseFromJson(data);
				tableHealthBar.removePlayer(removePlayerConnected(player.id));
//				websocket.showConsole(data);
			}
		});
	}

	public void addPlayerConnected(long id, PlayerOpponent opponent) {
		playersOpponents.put(id, opponent);
		tableHealthBar.addPlayer(opponent);
	}

	public PlayerOpponent removePlayerConnected(long id) {
		PlayerOpponent op = playersOpponents.get(id);
		world.destroyBody(op.getBody());
		return playersOpponents.remove(id);
	}

	public void updatePlayerConnected(PlayerJson playerJson) {
		PlayerOpponent player = playersOpponents.get(playerJson.id);
		if (player != null) {
			/*
			 * if(playerJson.input == 1) player.applyFrontForce(); else
			 * if(playerJson.input == 2) player.applyBackForce(); else
			 * if(playerJson.input == 3) player.applyJumpForce();
			 */
			player.setState(playerJson.state);
			player.setFlipRegion(playerJson.fliped);
			player.getBody().setTransform(playerJson.position, player.getBody().getAngle());
		}
	}

	public void addPlayerLocal(long id, String sessionId) {
		player = new Player(id,
				new PlayerConfiguration("Player_1", Input.Keys.Z, Input.Keys.X, Input.Keys.UP, Input.Keys.DOWN,
						Input.Keys.RIGHT, Input.Keys.LEFT, getWorld(), InputProcessorManagerSingleton.getInstance()));
		player.setSessionId(sessionId);
		tableHealthBar.addPlayer(player);
	}

	public PlayerJson parseFromJson(String json) {
		JsonValue jsonValue = new JsonReader().parse(json);
		return parseFromJson(jsonValue);
	}

	public PlayerJson parseFromJson(JsonValue jsonValue) {
		
		PlayerJson player = new PlayerJson();
		int indexState = jsonValue.getInt("state");

		player.id = jsonValue.getLong("id");
		player.position.x = ((Double) jsonValue.getDouble("x")).floatValue() / ScreenConstant.PPM;
		player.position.y = ((Double) jsonValue.getDouble("y")).floatValue() / ScreenConstant.PPM;
		player.sessionId = jsonValue.getString("sessionId");
		player.input = jsonValue.getInt("input");
		player.state = State.values()[indexState];
		player.fliped = jsonValue.getBoolean("fliped");

		return player;
	}

	public List<PlayerJson> parseFromJsonArray(String json) {

		List<PlayerJson> players = new ArrayList<>();

		JsonValue jsonValue = new JsonReader().parse(json);

		for (JsonValue j : jsonValue) {
			players.add(parseFromJson(j));
		}

		return players;
	}

	public String parseToJson(Player player) {
		return "{" + "\"id\":" + player.getId() + ",\"x\":"
				+ (double) player.getBody().getPosition().x * ScreenConstant.PPM + ",\"y\":"
				+ (double) player.getBody().getPosition().y * ScreenConstant.PPM + ",\"input\":"
				+ /* player.getCurrentInput() */ 1 + ",\"state\":" + player.currentState.ordinal() + ",\"fliped\":"
				+ player.isFlipX() + "}";
	}

	private class PlayerJson {
		long id;
		Vector2 position;
		String sessionId;
		State state;
		boolean fliped;
		int input;

		public PlayerJson() {
			position = new Vector2();
		}
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}
}