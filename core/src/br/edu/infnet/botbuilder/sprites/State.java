package br.edu.infnet.botbuilder.sprites;

public enum State {
	STANDING, JUMPING, RUNNING, FALLING, DEAD, ATTACKING
}
