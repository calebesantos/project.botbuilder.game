package br.edu.infnet.botbuilder.sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;

import br.edu.infnet.botbuilder.constants.BitCollisionConstant;
import br.edu.infnet.botbuilder.constants.ScreenConstant;

public class Player extends Sprite {
	
	public static final float MAX_HEALTH = 100;

	public State currentState;
	public State previousState;

	private float stateTimer;
	private boolean runningRight;
	private Body body;
	private PlayerConfiguration playerConfiguration;
	private static float positionX;
	private float life;
	
	private Vector2 previousPosition;
	private long id;
	private String sessionId;

	private boolean statusChanged;

	public float getLife() {
		return life;
	}

	public void setLife(float life) {
		this.life = life;
	}

	public Player(long id, PlayerConfiguration playerConfiguration) {

		if (positionX == 0)
			positionX = ScreenConstant.toPPM(100);
		else
			positionX += ScreenConstant.toPPM(100);

		this.playerConfiguration = playerConfiguration;
		this.id = id;
		
		currentState = State.STANDING;
		previousState = State.STANDING;
		stateTimer = 0;
		runningRight = true;
		life = MAX_HEALTH;		

		setBounds(0, 0, ScreenConstant.toPPM(50), ScreenConstant.toPPM(82));
		setRegion(playerConfiguration.getStandAnimation().getKeyFrames()[0]);
		defineBodyPlayer(getHeight(), new Vector2(positionX, ScreenConstant.toPPM(ScreenConstant.V_HEIGHT / 4)));
		
		previousPosition = new Vector2(getX(), getY());
	}

	public void update(float delta) {

		handleInput(delta);

		updatePosition();
		updateHandlers(delta, stateTimer);
		updateCurrentState();
		updateAnimation();
		
		if (currentState != previousState)
			updateFixture(stateTimer, currentState, runningRight);

		updateStates(delta);
	}

	private void updateAnimation() {
		setRegion(getFrame(currentState));
	}

	private void updateCurrentState() {
		
		currentState = getState();
		
		if(currentState != previousState) {
			statusChanged = true;
		} else {
			statusChanged = false;
		}
	}

	public TextureRegion getFrame(State state) {

		TextureRegion region;

		switch (state) {

		case ATTACKING:
			region = playerConfiguration.getCurrentAttackAnimation().getKeyFrame(stateTimer);
			break;
		case JUMPING:
			region = playerConfiguration.getJumpAnimation().getKeyFrame(stateTimer, true);
			break;
		case RUNNING:
			region = playerConfiguration.getWalkAnimation().getKeyFrame(stateTimer, true);
			break;
		default:
			region = playerConfiguration.getStandAnimation().getKeyFrame(stateTimer, true);
		}

		flipRight(region);

		return region;
	}

	private void flipRight(TextureRegion region) {
		if ((isKeyBackPressed() || !runningRight) && !region.isFlipX()) {
			region.flip(true, false);
			runningRight = false;

		} else if ((isKeyFrontPressed() || runningRight) && region.isFlipX()) {
			region.flip(true, false);
			runningRight = true;
		}
	}

	public State getState() {

		if (isAttacking())
			return State.ATTACKING;

		if (isJumping() || (isFalling() && previousState == State.JUMPING))
			return State.JUMPING;

		if (isFalling())
			return State.FALLING;

		if (isRunning())
			return State.RUNNING;

		return State.STANDING;
	}

	private void handleInput(float delta) {

		if (currentState != State.DEAD) {

			if (isKeyJumpPressed()){ applyJumpForce(); }

			else if (isKeyBackPressed()){ applyBackForce(); }

			else if (isKeyFrontPressed()){ applyFrontForce(); }
		}
	}

	public void hit(Player playerOpponent) {
		life -= 20;
	}
	
	public void hit(PlayerOpponent playerOpponent) {
		life -= 20;
	}
	
	private void defineBodyPlayer(float height, Vector2 position) {

		BodyDef bDef = new BodyDef();

		bDef.position.set(position);
		bDef.type = BodyType.DynamicBody;
		body = playerConfiguration.createBody(bDef);

		FixtureDef fixtureDef = new FixtureDef();
		PolygonShape shape = new PolygonShape();
		Vector2 positionShape = new Vector2(body.getLocalCenter());

		float heightSprite = height / 2;
		float widthSprite = ScreenConstant.toPPM(16);

		fixtureDef.filter.categoryBits = BitCollisionConstant.PLAYER_BIT;
		fixtureDef.filter.maskBits = BitCollisionConstant.GROUND_BIT | BitCollisionConstant.PLAYER_BIT
				| BitCollisionConstant.PLAYER_ARM_BIT;
		fixtureDef.restitution = 0f;
		fixtureDef.friction = 1f;
		shape.setAsBox(widthSprite, heightSprite, positionShape, 0);

		fixtureDef.shape = shape;

		body.createFixture(fixtureDef).setUserData(this);

		fixtureDef.filter.categoryBits = BitCollisionConstant.PLAYER_ARM_BIT;
		shape.setAsBox(ScreenConstant.toPPM(3), ScreenConstant.toPPM(3));

		body.createFixture(fixtureDef).setUserData(this);
		//
		// fixtureDef.filter.categoryBits = BitCollisionConstant.PLAYER_BIT;
		// fixtureDef.filter.maskBits = BitCollisionConstant.GROUND_BIT;
		//
		// fixtureDef.shape = shape;
		// body.createFixture(fixtureDef).setUserData(this);
		//
		// positionShape.y += heightSprite * 2;
		// shape.setAsBox(widthSprite, heightSprite, positionShape, 0);
		// body.createFixture(fixtureDef).setUserData(this);
		//
		// positionShape.y += heightSprite * 2;
		// shape.setAsBox(widthSprite, heightSprite, positionShape, 0);
		// body.createFixture(fixtureDef).setUserData(this);
	}
	
	public void updateFixture(float stateTime, State state, boolean runningRight) {

		PolygonShape shape = null;

		for (Fixture fixture : body.getFixtureList()) {

			int categoryBit = fixture.getFilterData().categoryBits;
			shape = (PolygonShape) fixture.getShape();

			switch (categoryBit) {

			case BitCollisionConstant.PLAYER_ARM_BIT:
				updateArmFixture(stateTime, shape, state, runningRight);
				break;

			default:
				break;
			}
		}

		switch (state) {
		case STANDING:

			break;

		default:
			break;
		}

	}

	public void updateArmFixture(float stateTime, PolygonShape shape, State state, boolean runningRight) {

		Vector2 position = new Vector2(body.getLocalCenter());

		if (state == State.ATTACKING) {

			if (isKeyPunchPressed()) {

				if (isComboDetected()) {

					if (runningRight)
						position.x += ScreenConstant.toPPM(22);
					else
						position.x -= ScreenConstant.toPPM(23);

					position.y += ScreenConstant.toPPM(24);
				} else {

					if (runningRight)
						position.x += ScreenConstant.toPPM(21);
					else
						position.x -= ScreenConstant.toPPM(21);

					position.y += ScreenConstant.toPPM(25);
				}
			} else if (isKeyKickPressed()) {

				if (isComboDetected()) {

					if (runningRight)
						position.x += ScreenConstant.toPPM(24);
					else
						position.x -= ScreenConstant.toPPM(21);

					position.y += ScreenConstant.toPPM(36);

				} else {

					if (runningRight)
						position.x += ScreenConstant.toPPM(24);
					else
						position.x -= ScreenConstant.toPPM(21);

					position.y += ScreenConstant.toPPM(36);
				}
			}
		}

		shape.setAsBox(ScreenConstant.toPPM(3), ScreenConstant.toPPM(3), position, 0);
	}
	
	private boolean isComboDetected() {
		return playerConfiguration.getComboHandler().isComboDetected();
	}

	private boolean isKeyPunchPressed() {
		return Gdx.input.isKeyJustPressed(playerConfiguration.getKeyPunch());
	}

	private boolean isKeyKickPressed() {
		return Gdx.input.isKeyJustPressed(playerConfiguration.getKeyKick());
	}

	private void updateHandlers(float delta, float stateTimer) {
		playerConfiguration.getAttackHandler().update(delta, stateTimer);
		playerConfiguration.getComboHandler().update(delta, stateTimer);
	}
	
	private void updateStates(float delta) {
		stateTimer = currentState == previousState ? stateTimer + delta : 0;
		previousState = currentState;
	}
	
	private void updatePosition() {
		setPosition(getBody().getPosition().x - getWidth() / 2, getBody().getPosition().y - getHeight() / 2);
	}
	
	public boolean isAttacking() {
		return  playerConfiguration.getAttackHandler().isAttackDetected() || playerConfiguration.getComboHandler().isComboDetected();
	}
	
	public boolean isJumping() {
		return getBody().getLinearVelocity().y > 0;
	}
	
	public boolean isFalling() {
		return getBody().getLinearVelocity().y < 0;
	}

	public boolean isRunning() {
		return getBody().getLinearVelocity().x != 0;
	}

	public void applyJumpForce() {
		getBody().applyLinearImpulse(playerConfiguration.getJumpForce(), getBody().getWorldCenter(), true);
	}
	
	public void applyFrontForce() {
		getBody().applyLinearImpulse(playerConfiguration.getFrontForce(), getBody().getWorldCenter(), true);
	}

	public void applyBackForce() {
		getBody().applyLinearImpulse(playerConfiguration.getBackForce(), getBody().getWorldCenter(), true);
	}
	
	public boolean isKeyJumpPressed() {
		return Gdx.input.isKeyJustPressed(playerConfiguration.getKeyUp());
	}
	
	public boolean isKeyFrontPressed() {
		return Gdx.input.isKeyPressed(playerConfiguration.getKeyFront()) && getBody().getLinearVelocity().x <= 1.5;
	}

	public boolean isKeyBackPressed() {
		return Gdx.input.isKeyPressed(playerConfiguration.getKeyBack()) && getBody().getLinearVelocity().x >= -1.5;
	}
	
	public Body getBody() {
		return body;
	}
	
	public boolean hasMoved() {
		
        if(previousPosition.x == getX() && previousPosition.y == getY())
            return false;

        previousPosition.x = getX();
        previousPosition.y = getY();
        return true;
    }

	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public int getCurrentInput() {
		
		if(isKeyFrontPressed()) return 1;

		if (isKeyBackPressed()) return 2;
		
		if (isKeyJumpPressed()) return 3;
		
		if (isKeyPunchPressed()) return 4;
		
		return 5;
	}

	public boolean hasChangedStatus() {
		return hasMoved() || hasStateChanged();
	}
	
	public boolean hasStateChanged() {
		return statusChanged;
	}

}
