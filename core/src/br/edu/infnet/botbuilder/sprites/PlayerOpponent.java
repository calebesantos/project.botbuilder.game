package br.edu.infnet.botbuilder.sprites;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;

import br.edu.infnet.botbuilder.constants.BitCollisionConstant;
import br.edu.infnet.botbuilder.constants.ScreenConstant;

public class PlayerOpponent extends Sprite {

	public static final float MAX_HEALTH = 100;

	private float life;
	private PlayerConfiguration playerConfiguration;
	private Body body;
	private String sessionId;
	private long id;
	private float stateTimer;

	private boolean flipRegion;

	private State previousState;

	private State currentState;

	public PlayerOpponent(PlayerConfiguration playerConfiguration) {
		this.playerConfiguration = playerConfiguration;
		life = MAX_HEALTH;
		previousState = currentState = State.STANDING;

		setBounds(0, 0, ScreenConstant.toPPM(50), ScreenConstant.toPPM(82));
		setRegion(playerConfiguration.getStandAnimation().getKeyFrames()[0]);
		defineBodyPlayer(getHeight(),
				new Vector2(ScreenConstant.toPPM(50), ScreenConstant.toPPM(ScreenConstant.V_HEIGHT / 4)));
	}

	public void setLife(float life) {
		this.life = life;
	}

	public float getLife() {
		return life;
	}

	private void defineBodyPlayer(float height, Vector2 position) {

		BodyDef bDef = new BodyDef();

		bDef.position.set(position);
		bDef.type = BodyType.KinematicBody;
		body = playerConfiguration.createBody(bDef);

		FixtureDef fixtureDef = new FixtureDef();
		PolygonShape shape = new PolygonShape();
		Vector2 positionShape = new Vector2(body.getLocalCenter());

		float heightSprite = height / 2;
		float widthSprite = ScreenConstant.toPPM(16);

		fixtureDef.filter.categoryBits = BitCollisionConstant.PLAYER_BIT;
		fixtureDef.filter.maskBits = BitCollisionConstant.GROUND_BIT | BitCollisionConstant.PLAYER_BIT
				| BitCollisionConstant.PLAYER_ARM_BIT;
		fixtureDef.restitution = 0f;
		fixtureDef.friction = 1f;
		shape.setAsBox(widthSprite, heightSprite, positionShape, 0);

		fixtureDef.shape = shape;

		body.createFixture(fixtureDef).setUserData(this);

		fixtureDef.filter.categoryBits = BitCollisionConstant.PLAYER_ARM_BIT;
		shape.setAsBox(ScreenConstant.toPPM(3), ScreenConstant.toPPM(3));

		body.createFixture(fixtureDef).setUserData(this);
		//
		// fixtureDef.filter.categoryBits = BitCollisionConstant.PLAYER_BIT;
		// fixtureDef.filter.maskBits = BitCollisionConstant.GROUND_BIT;
		//
		// fixtureDef.shape = shape;
		// body.createFixture(fixtureDef).setUserData(this);
		//
		// positionShape.y += heightSprite * 2;
		// shape.setAsBox(widthSprite, heightSprite, positionShape, 0);
		// body.createFixture(fixtureDef).setUserData(this);
		//
		// positionShape.y += heightSprite * 2;
		// shape.setAsBox(widthSprite, heightSprite, positionShape, 0);
		// body.createFixture(fixtureDef).setUserData(this);
	}

	public void updateFixture(float stateTime, State state, boolean runningRight) {

		PolygonShape shape = null;

		for (Fixture fixture : body.getFixtureList()) {

			int categoryBit = fixture.getFilterData().categoryBits;
			shape = (PolygonShape) fixture.getShape();

			switch (categoryBit) {

			case BitCollisionConstant.PLAYER_ARM_BIT:
				updateArmFixture(stateTime, shape, state, runningRight);
				break;

			default:
				break;
			}
		}

		switch (state) {
		case STANDING:

			break;

		default:
			break;
		}

	}

	public void updateArmFixture(float stateTime, PolygonShape shape, State state, boolean runningRight) {

		/*
		 * Vector2 position = new Vector2(body.getLocalCenter());
		 * 
		 * if (state == State.ATTACKING) {
		 * 
		 * if (isKeyPunchPressed()) {
		 * 
		 * if (isComboDetected()) {
		 * 
		 * if (runningRight) position.x += ScreenConstant.toPPM(22); else
		 * position.x -= ScreenConstant.toPPM(23);
		 * 
		 * position.y += ScreenConstant.toPPM(24); } else {
		 * 
		 * if (runningRight) position.x += ScreenConstant.toPPM(21); else
		 * position.x -= ScreenConstant.toPPM(21);
		 * 
		 * position.y += ScreenConstant.toPPM(25); } } else if
		 * (isKeyKickPressed()) {
		 * 
		 * if (isComboDetected()) {
		 * 
		 * if (runningRight) position.x += ScreenConstant.toPPM(24); else
		 * position.x -= ScreenConstant.toPPM(21);
		 * 
		 * position.y += ScreenConstant.toPPM(36);
		 * 
		 * } else {
		 * 
		 * if (runningRight) position.x += ScreenConstant.toPPM(24); else
		 * position.x -= ScreenConstant.toPPM(21);
		 * 
		 * position.y += ScreenConstant.toPPM(36); } } }
		 * 
		 * shape.setAsBox(ScreenConstant.toPPM(3), ScreenConstant.toPPM(3),
		 * position, 0);
		 */
	}

	public void hit(Player playerOpponent) {
		life -= 20;
	}

	public void hit(PlayerOpponent playerOpponent) {
		life -= 20;
	}

	public void update(float delta) {
		updatePosition();
		updateStateTimer(delta);
		updateAnimation(delta, stateTimer);
	}

	public void updateAnimation(float delta, float stateTimer) {
		setRegion(getFrame(currentState));
	}

	public void updateStateTimer(float delta) {
		stateTimer = currentState == previousState ? stateTimer + delta : 0;
		previousState = currentState;
	}

	private void updatePosition() {
		setPosition(body.getPosition().x - getWidth() / 2, body.getPosition().y - getHeight() / 2);
	}

	public void setPosition(Vector2 position) {
		setPosition(position);
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Body getBody() {
		return body;
	}

	public TextureRegion getFrame(State state) {

		TextureRegion region;

		switch (state) {

		case ATTACKING:
			region = playerConfiguration.getPunchAnimation().getKeyFrame(stateTimer);// playerConfiguration.getCurrentAttackAnimation().getKeyFrame(stateTimer);
			break;
		case JUMPING:
			region = playerConfiguration.getJumpAnimation().getKeyFrame(stateTimer, true);
			break;
		case RUNNING:
			region = playerConfiguration.getWalkAnimation().getKeyFrame(stateTimer, true);
			break;
		default:
			region = playerConfiguration.getStandAnimation().getKeyFrame(stateTimer, true);
		}

		if (flipRegion() && !this.isFlipX()) {
			region.flip(true, false);
		} else if (!flipRegion() && this.isFlipX()) {
			region.flip(true, false);
		}

		return region;
	}

	public boolean flipRegion() {
		return flipRegion;
	}

	public void setFlipRegion(boolean flipRegion) {
		this.flipRegion = flipRegion;
	}

	public void setState(State state) {
		currentState = state;
	}

	/*
	 * public void applyFrontForce() {
	 * getBody().applyLinearImpulse(playerConfiguration.getFrontForce(),
	 * getBody().getWorldCenter(), true); }
	 * 
	 * public void applyJumpForce() {
	 * getBody().applyLinearImpulse(playerConfiguration.getJumpForce(),
	 * getBody().getWorldCenter(), true); }
	 * 
	 * public void applyBackForce() {
	 * getBody().applyLinearImpulse(playerConfiguration.getBackForce(),
	 * getBody().getWorldCenter(), true); }
	 */

}
