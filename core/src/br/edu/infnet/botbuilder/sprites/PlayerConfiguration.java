package br.edu.infnet.botbuilder.sprites;

import java.util.HashMap;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.World;

import br.edu.infnet.botbuilder.factory.AnimationFactory;
import br.edu.infnet.botbuilder.tools.AttackHandler;
import br.edu.infnet.botbuilder.tools.ComboHandler;
import br.edu.infnet.botbuilder.tools.InputProcessorManager;

public class PlayerConfiguration {

	private int keyPunch;
	private int keyKick;
	private int keyUp;
	private int keyDown;
	private int keyFront;
	private int keyBack;
	private String playerName;
	private World world;

	private ComboHandler comboHandler;

	private AnimationFactory animationFactory;

	private Animation ryuStand;
	private Animation ryuWalking;
	private Animation ryuJump;
	private Animation ryuPunch;
	private Animation ryuKick;
	private Animation ryuHighPunch;
	private Animation ryuHighPunchFliped;
	private Animation ryuHighKick;
	private Animation ryuHighKickFliped;
	private AttackHandler attackHandler;

	private Vector2 jumpForce;
	private Vector2 frontForce;
	private Vector2 backForce;

	public PlayerConfiguration(String pathProperties, World world, InputProcessorManager inputProcessorManager) {
		this(world, inputProcessorManager);
	} 
	
	public PlayerConfiguration(World world, InputProcessorManager inputProcessorManager) {

		this.world = world;
		this.animationFactory = new AnimationFactory();

		ryuStand = animationFactory.getAnimation("botbuilder/ryu_sprites/ryu_idle.png", 0, 0, 50, 82, 0.1f);
		ryuWalking = animationFactory.getAnimation("botbuilder/ryu_sprites/ryu_walking.png", 0, 0, 49, 81, 0.1f);
		ryuJump = animationFactory.getAnimation("botbuilder/ryu_sprites/ryu_jump.png", 0, 0, 42, 90, 0.1f);
		ryuPunch = animationFactory.getAnimation("botbuilder/ryu_sprites/ryu_low_punching.png", 0, 0, 55, 81, 0.1f);
		ryuKick = animationFactory.getAnimation("botbuilder/ryu_sprites/ryu_low_kick.png", 0, 0, 63, 87, 0.15f);

		comboHandler = new ComboHandler(generateCombos());
		attackHandler = new AttackHandler(generateAttacks());

		jumpForce = new Vector2(0, 3.5f);
		frontForce = new Vector2(0.5f, 0);
		backForce = new Vector2(-0.5f, 0);

		inputProcessorManager.addInputProcessor(attackHandler);
		inputProcessorManager.addInputProcessor(comboHandler);
	}

	public PlayerConfiguration(String playerName, int keyPunch, int keyKick, int keyUp, int keyDown, int keyFront,
			int keyBack, World world, InputProcessorManager inputProcessorManager) {
		// this(world, inputProcessorManager);
		this.playerName = playerName;
		this.keyPunch = keyPunch;
		this.keyKick = keyKick;
		this.keyUp = keyUp;
		this.keyDown = keyDown;
		this.keyFront = keyFront;
		this.keyBack = keyBack;

		this.world = world;
		this.animationFactory = new AnimationFactory();

		ryuStand = animationFactory.getAnimation("botbuilder/ryu_sprites/ryu_idle.png", 0, 0, 50, 82, 0.1f);
		ryuWalking = animationFactory.getAnimation("botbuilder/ryu_sprites/ryu_walking.png", 0, 0, 49, 81, 0.1f);
		ryuJump = animationFactory.getAnimation("botbuilder/ryu_sprites/ryu_jump.png", 0, 0, 42, 90, 0.1f);
		ryuPunch = animationFactory.getAnimation("botbuilder/ryu_sprites/ryu_low_punching.png", 0, 0, 55, 81, 0.1f);
		ryuKick = animationFactory.getAnimation("botbuilder/ryu_sprites/ryu_low_kick.png", 0, 0, 63, 87, 0.15f);
		
		ryuHighPunch = animationFactory.getAnimation("botbuilder/ryu_sprites/ryu_high_punching.png", 0, 0, 60, 85,
				0.1f);
		ryuHighPunchFliped = animationFactory.getAnimation("botbuilder/ryu_sprites/ryu_high_punching.png", 0, 0, 58, 85,
				0.1f, true, false);
		ryuHighKick = animationFactory.getAnimation("botbuilder/ryu_sprites/ryu_high_kick.png", 0, 0, 58, 85, 0.1f,
				true, false);
		ryuHighKickFliped = animationFactory.getAnimation("botbuilder/ryu_sprites/ryu_high_kick.png", 0, 0, 58, 85,
				0.1f, true, false);

		comboHandler = new ComboHandler(generateCombos());
		attackHandler = new AttackHandler(generateAttacks());

		jumpForce = new Vector2(0, 3.5f);
		frontForce = new Vector2(0.5f, 0);
		backForce = new Vector2(-0.5f, 0);

		inputProcessorManager.addInputProcessor(attackHandler);
		inputProcessorManager.addInputProcessor(comboHandler);
	}

	public int getKeyPunch() {
		return keyPunch;
	}

	public void setKeyPunch(int keyPunch) {
		this.keyPunch = keyPunch;
	}

	public int getKeyKick() {
		return keyKick;
	}

	public void setKeyKick(int keyKick) {
		this.keyKick = keyKick;
	}

	public int getKeyUp() {
		return keyUp;
	}

	public void setKeyUp(int keyUp) {
		this.keyUp = keyUp;
	}

	public int getKeyDown() {
		return keyDown;
	}

	public void setKeyDown(int keyDown) {
		this.keyDown = keyDown;
	}

	public int getKeyFront() {
		return keyFront;
	}

	public void setKeyFront(int keyFront) {
		this.keyFront = keyFront;
	}

	public int getKeyBack() {
		return keyBack;
	}

	public void setKeyBack(int keyBack) {
		this.keyBack = keyBack;
	}

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public HashMap<String, Animation> generateCombos() {

		HashMap<String, Animation> combos = new HashMap<>();

		// Hadouken
		StringBuilder hadoukenComboKeys = new StringBuilder();
		hadoukenComboKeys.append(getKeyDown());
		hadoukenComboKeys.append(",");
		hadoukenComboKeys.append(getKeyFront());
		hadoukenComboKeys.append(",");
		hadoukenComboKeys.append(getKeyPunch());
		combos.put(hadoukenComboKeys.toString(), ryuHighPunch);

		hadoukenComboKeys.setLength(0);
		hadoukenComboKeys.append(getKeyDown());
		hadoukenComboKeys.append(",");
		hadoukenComboKeys.append(getKeyBack());
		hadoukenComboKeys.append(",");
		hadoukenComboKeys.append(getKeyPunch());
		combos.put(hadoukenComboKeys.toString(), ryuHighPunchFliped);

		hadoukenComboKeys.setLength(0);
		hadoukenComboKeys.append(getKeyDown());
		hadoukenComboKeys.append(",");
		hadoukenComboKeys.append(getKeyFront());
		hadoukenComboKeys.append(",");
		hadoukenComboKeys.append(getKeyKick());
		combos.put(hadoukenComboKeys.toString(), ryuHighKick);

		hadoukenComboKeys.setLength(0);
		hadoukenComboKeys.append(getKeyDown());
		hadoukenComboKeys.append(",");
		hadoukenComboKeys.append(getKeyBack());
		hadoukenComboKeys.append(",");
		hadoukenComboKeys.append(getKeyKick());
		combos.put(hadoukenComboKeys.toString(), ryuHighKickFliped);

		return combos;
	}

	public HashMap<Integer, Animation> generateAttacks() {

		HashMap<Integer, Animation> attacks = new HashMap<>();

		attacks.put(getKeyPunch(), ryuPunch);
		attacks.put(getKeyKick(), ryuKick);

		return attacks;
	}

	public Animation getStandAnimation() {
		return ryuStand;
	}

	public void setRyuStand(Animation ryuStand) {
		this.ryuStand = ryuStand;
	}

	public Animation getWalkAnimation() {
		return ryuWalking;
	}
	
	public Animation getPunchAnimation() {
		return ryuPunch;
	}

	public void setRyuWalking(Animation ryuWalking) {
		this.ryuWalking = ryuWalking;
	}

	public Animation getJumpAnimation() {
		return ryuJump;
	}

	public void setRyuJump(Animation ryuJump) {
		this.ryuJump = ryuJump;
	}

	public Animation getRyuPunch() {
		return ryuPunch;
	}

	public void setRyuPunch(Animation ryuPunch) {
		this.ryuPunch = ryuPunch;
	}

	public Animation getRyuKick() {
		return ryuKick;
	}

	public void setRyuKick(Animation ryuKick) {
		this.ryuKick = ryuKick;
	}

	public AttackHandler getAttackHandler() {
		return attackHandler;
	}

	public ComboHandler getComboHandler() {
		return comboHandler;
	}

	public Animation getCurrentAttackAnimation() {
		return comboHandler.isComboDetected() ? comboHandler.getCurrentAnimation()
				: attackHandler.getCurrentAnimation();
	}

	public Vector2 getBackForce() {
		return backForce;
	}

	public Vector2 getFrontForce() {
		return frontForce;
	}

	public Vector2 getJumpForce() {
		return jumpForce;
	}

	public Body createBody(BodyDef bDef) {
		return world.createBody(bDef);
	}
}
