package br.edu.infnet.botbuilder.tools;

public interface SubscribeListener {
	void call(String data);
}
