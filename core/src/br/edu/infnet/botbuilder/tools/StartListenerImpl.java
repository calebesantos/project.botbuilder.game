package br.edu.infnet.botbuilder.tools;

import java.util.HashMap;

import br.edu.infnet.botbuilder.sprites.Player;

public class StartListenerImpl implements StartListener {

	private final WebsocketCustom websocket;
	private final Player player;
	private final HashMap<Long,Player> playersOpponents;
	
	public StartListenerImpl(WebsocketCustom websocket, Player player, HashMap<Long,Player> playersOpponents){
		this.websocket = websocket;
		this.player = player;
		this.playersOpponents = playersOpponents;
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub
		
	}

}
