package br.edu.infnet.botbuilder.tools;

public class WebsocketCustom {

    private String url;
    private String stompString;

    public WebsocketCustom(String url){
        this.url = url;
    }
    
    public void initialize(){ _initialize(); }
    public void connect(StartListener startListener){ _connect(startListener); }
    public void subscribe(String url, SubscribeListener subscribeListener){ _subscribe(url, subscribeListener); }
    public void send(String url, String message){ _send(url, message); }

    public native void alert(String msg)/*-{
        $wnd.alert(msg);
    }-*/;

    private native void _initialize()/*-{
        var value = this.@br.edu.infnet.botbuilder.tools.WebsocketCustom::url;
        var client = new $wnd.SockJS(value);
        var stompInitialize;
        $wnd.stompInitialize = $wnd.Stomp.over(client);
        $wnd.stompInitialize.debug = null

    }-*/;

    private native void _connect(StartListener startListener)/*-{
        $wnd.stompInitialize.connect({}, function(frame){
            startListener.@br.edu.infnet.botbuilder.tools.StartListener::init()();
        });
    }-*/;

    private native void _subscribe(String url, SubscribeListener subscribeListener)/*-{
        $wnd.stompInitialize.subscribe(url, function(data){
            subscribeListener.@br.edu.infnet.botbuilder.tools.SubscribeListener::call(Ljava/lang/String;)(data.body);
        });
    }-*/;

    private native void _send(String url, String message)/*-{
        $wnd.stompInitialize.send(url, {}, message);
    }-*/;

    public native void showConsole(String msg)/*-{
        $wnd.console.log(msg);
    }-*/;
}