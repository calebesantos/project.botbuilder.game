package br.edu.infnet.botbuilder.tools;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;

import br.edu.infnet.botbuilder.constants.BitCollisionConstant;
import br.edu.infnet.botbuilder.sprites.Player;
import br.edu.infnet.botbuilder.sprites.PlayerOpponent;

public class WorldContactListener implements ContactListener {

	@Override
	public void beginContact(Contact contact) {

		Fixture fixtureA = contact.getFixtureA();
		Fixture fixtureB = contact.getFixtureB();

		int contactBits = fixtureA.getFilterData().categoryBits | fixtureB.getFilterData().categoryBits;

		switch (contactBits) {
		case BitCollisionConstant.PLAYER_ARM_BIT | BitCollisionConstant.PLAYER_BIT:

			if (fixtureA.getFilterData().categoryBits == BitCollisionConstant.PLAYER_BIT) {

				if (fixtureB.getUserData() instanceof Player)
					((PlayerOpponent) fixtureA.getUserData()).hit((Player) fixtureB.getUserData());
				else
					((Player) fixtureA.getUserData()).hit((PlayerOpponent) fixtureB.getUserData());
			} else {
				
				if (fixtureA.getUserData() instanceof Player)
					((PlayerOpponent) fixtureB.getUserData()).hit((Player) fixtureA.getUserData());
				else
					((Player) fixtureB.getUserData()).hit((PlayerOpponent) fixtureA.getUserData());
			}
			break;
		default:
			break;
		}
	}

	@Override
	public void endContact(Contact contact) {
	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
	}

}
