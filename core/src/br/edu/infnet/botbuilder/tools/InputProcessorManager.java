package br.edu.infnet.botbuilder.tools;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.utils.Array;

public class InputProcessorManager implements InputProcessor {

	private Array<InputProcessor> inputProcessors;

	public InputProcessorManager() {
		inputProcessors = new Array<>();
	}

	public void addInputProcessor(InputProcessor inputProcessor) {
		inputProcessors.add(inputProcessor);
	}

	@Override
	public boolean keyDown(int keycode) {

		for (InputProcessor inputProcessor : inputProcessors) {
			inputProcessor.keyDown(keycode);
		}

		return false;
	}

	@Override
	public boolean keyUp(int keycode) {

		for (InputProcessor inputProcessor : inputProcessors) {
			inputProcessor.keyUp(keycode);
		}

		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		
		for (InputProcessor inputProcessor : inputProcessors) {
			inputProcessor.keyTyped(character);
		}
		
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		
		for (InputProcessor inputProcessor : inputProcessors) {
			inputProcessor.touchDown(screenX, screenY, pointer, button);
		}
		
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		
		for (InputProcessor inputProcessor : inputProcessors) {
			inputProcessor.touchUp(screenX, screenY, pointer, button);
		}
		
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		
		for (InputProcessor inputProcessor : inputProcessors) {
			inputProcessor.touchDragged(screenX, screenY, pointer);
		}
		
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		
		for (InputProcessor inputProcessor : inputProcessors) {
			inputProcessor.mouseMoved(screenX, screenY);
		}
		
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		
		for (InputProcessor inputProcessor : inputProcessors) {
			inputProcessor.scrolled(amount);
		}
		
		return false;
	}

}