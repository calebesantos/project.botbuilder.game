package br.edu.infnet.botbuilder.tools;

import java.util.HashMap;
import java.util.Map.Entry;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.g2d.Animation;

public class ComboHandler implements InputProcessor {

	private static final float LIMIT_TIME_COMBO = 0.19f;

	private HashMap<String, Animation> combos;
	private Animation animationCombo;

	private StringBuilder currentComboKeys;
	// private String previousComboKeys;
	private String lastComboKeys;

	private float intervalTimeCombo;

	private boolean comboDetected;

	public ComboHandler() {
		combos = new HashMap<>();
	}
	
	public ComboHandler(HashMap<String, Animation> combos) {
		this.combos = combos;
		currentComboKeys = new StringBuilder();
		comboDetected = false;
		lastComboKeys = "";
	}

	public void setCombo(String comboKeys, Animation animation) {
		combos.put(comboKeys, animation);
	}
	
	private void checkCombos() {

		for (Entry<String, Animation> entry : combos.entrySet()) {

			if (lastComboKeys.equals(entry.getKey())) {

				comboDetected = true;

				lastComboKeys = currentComboKeys.toString();
				animationCombo = entry.getValue();
				intervalTimeCombo = 0;

				return;
			}
		}
	}

	private void resetComboKeys() {
		currentComboKeys.setLength(0);
		lastComboKeys = currentComboKeys.toString();
		intervalTimeCombo = 0;
	}

	public boolean isComboDetected() {
		return comboDetected;
	}

	public Animation getCurrentAnimation() {
		return animationCombo;
	}

	public void update(float delta, float stateTime) {

		intervalTimeCombo += delta;

		if (!comboDetected) {

			checkCombos();

			if (intervalTimeCombo >= LIMIT_TIME_COMBO)
				resetComboKeys();

		} else if (animationCombo.isAnimationFinished(stateTime)) {
			comboDetected = false;
			resetComboKeys();
		}

	}

	@Override
	public boolean keyDown(int keycode) {

		if (!comboDetected) {

			if (currentComboKeys.length() > 0)
				currentComboKeys.append(",");

			currentComboKeys.append(keycode);

			lastComboKeys = currentComboKeys.toString();
		}

		intervalTimeCombo = 0;

		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}
}
