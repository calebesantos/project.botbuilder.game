package br.edu.infnet.botbuilder.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import br.edu.infnet.botbuilder.BotBuilder;
import br.edu.infnet.botbuilder.constants.ScreenConstant;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = ScreenConstant.V_WIDTH;
		config.height = ScreenConstant.V_HEIGHT;
		new LwjglApplication(new BotBuilder(), config);
	}
}
